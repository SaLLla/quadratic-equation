﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuadraticEquation
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] Index = new double[3];

            char x = 'a';
            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine("Введите {0}", x);
                x++;

                Index[i] = Convert.ToDouble(Console.ReadLine());
            }

            Determinant a = new Determinant(Index[0], Index[1], Index[2]);
            a.DetOut();
            Console.ReadLine();
        }
    }
    class Determinant
    {
        double aObj, bObj, cObj;

        public Determinant(double a, double b, double c)
        {
            aObj = a;
            bObj = b;
            cObj = c;
        }
        
        private double Det()
        {
            return Math.Pow(bObj, 2) - 4 * aObj * cObj; // вычисление дискриминанта
        }

        private double Root()
        {
            return -bObj / 2 * aObj; // вычисление корня, при дискриминанте равном нулю
        }

        private double Root(out double x1) // вычисление корня, при дискриминате больше нуля. Клево, да? :)
        {
            x1 = (-bObj + Math.Sqrt(Det())) / 2 * aObj;
            return (-bObj - Math.Sqrt(Det())) / 2 * aObj;
        }

        public void DetOut() 
        {
            if (Det() < 0)
            {
                Console.WriteLine("Вещественных корней нет");
            }

            if (Det() > 0)
            {
                double x1;
                Console.WriteLine("x1 = {0}, x2 = {1}", Root(out x1), x1);
            }

            if (Det() == 0)
            {
                Console.WriteLine("x1 = x2 = {0}", Root());
            }
        }
    }
}

